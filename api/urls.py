from django.urls import path
from api.views import MetricCLAPIView,MetricRUDAPIView

urlpatterns = [
    path('metric/<int:pk>/',  MetricRUDAPIView.as_view()),
    path('metric/', MetricCLAPIView.as_view()),
]