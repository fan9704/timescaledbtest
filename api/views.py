import datetime
import time

from django.contrib.auth.models import User
from django.contrib import auth
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets, mixins, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import MetricSerializer
from api import models

# Create your views here.
class MetricRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Metric.objects.all()
    serializer_class = MetricSerializer


class MetricCLAPIView(generics.ListCreateAPIView):
    queryset = models.Metric.objects.all()
    serializer_class = MetricSerializer